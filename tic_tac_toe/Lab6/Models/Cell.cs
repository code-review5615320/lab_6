﻿namespace Lab6.Models
{
    public class Cell
    {
        public char Value { get; set; }

        public Cell()
        {
            Value = ' ';
        }

        public bool IsEmpty()
        {
            return Value == ' ';
        }
    }
}
