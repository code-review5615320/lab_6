﻿using Lab6.Models.AI;

namespace Lab6.Models.Player
{
    public class ComputerPlayer : IPlayer
    {
        private char player;
        private AIBase ai;

        public ComputerPlayer(char player, AIBase ai)
        {
            this.player = player;
            this.ai = ai;
        }

        public (int, int) GetMove(Board board, char player)
        {
            return ai.GetMove(player);
        }
    }

}
