﻿using System;

namespace Lab6.Models.Player
{
    public class HumanPlayer : IPlayer
    {
        private char player;

        public HumanPlayer(char player)
        {
            this.player = player;
        }

        public (int, int) GetMove(Board board, char player)
        {

            Console.Write("Enter row (0, 1, 2): ");
            int row;
            while (!int.TryParse(Console.ReadLine(), out row) || row < 0 || row > 2)
            {
                Console.WriteLine("Invalid number. Please retry.");
            }
            Console.Write("Enter column (0, 1, 2): ");
            int col;
            while (!int.TryParse(Console.ReadLine(), out col) || col < 0 || col > 2)
            {
                Console.WriteLine("Invalid number. Please retry.");
            }

            return (row, col);
        }
    }

}
