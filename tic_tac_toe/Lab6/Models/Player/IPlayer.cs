﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Models.Player
{
    public interface IPlayer
    {
        (int, int) GetMove(Board board, char player);
    }

}
