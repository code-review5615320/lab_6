﻿namespace Lab6.Models.AI
{
    public class EasyAI : AIBase
    {
        public EasyAI(Board board) : base(board, "easy") { }

        public override (int, int) GetMove(char currentPlayer)
        {
            CreateBoardCopy();
            return GetRandomMove();
        }
    }


}
