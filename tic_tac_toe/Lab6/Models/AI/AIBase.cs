﻿using System;

namespace Lab6.Models.AI
{
    public abstract class AIBase
    {
        protected Board board;
        protected Board boardCopy;
        protected string difficulty;

        public AIBase(Board board, string difficulty)
        {
            this.board = board;
            CreateBoardCopy();
            this.difficulty = difficulty;
        }

        public void CreateBoardCopy()
        {
            boardCopy = new Board(board.board);
        }

        public abstract (int, int) GetMove(char currentPlayer);

        protected (int, int) GetRandomMove()
        {
            Random rnd = new Random();
            int row, col;

            do
            {
                row = rnd.Next(0, 3);
                col = rnd.Next(0, 3);
            } while (!boardCopy.IsEmpty(row, col));

            return (row, col);
        }

        protected bool TryToBlockPlayer(out (int, int) move)
        {
            return TryToWin('X', out move);
        }

        protected bool TryToWin(char player, out (int, int) move)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (boardCopy.IsEmpty(i, j))
                    {
                        boardCopy.MakeMove(i, j, player);
                        bool win = boardCopy.CheckWin(player);
                        boardCopy.UndoMove(i, j);
                        if (win)
                        {
                            move = (i, j);
                            return true;
                        }
                    }
                }
            }
            move = (-1, -1);
            return false;
        }
    }


}
