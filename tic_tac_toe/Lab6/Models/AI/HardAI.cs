﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Models.AI
{

    public class HardAI : AIBase
    {
        public HardAI(Board board) : base(board, "hard") { }

        public override (int, int) GetMove(char currentPlayer)
        {
            CreateBoardCopy();

            if (TryToWin(currentPlayer, out var move))
            {
                return move;
            }
            if (TryToBlockPlayer(out move))
            {
                return move;
            }
            return GetRandomMove();
        }
    }


}
