﻿namespace Lab6.Models.AI
{
    public class MediumAI : AIBase
    {
        public MediumAI(Board board) : base(board, "medium") { }

        public override (int, int) GetMove(char currentPlayer)
        {
            CreateBoardCopy();
            if (TryToBlockPlayer(out var move))
            {
                return move;
            }
            return GetRandomMove();
        }
    }


}
