﻿using Lab6.Models.AI;
using Lab6.Models.Player;
using System;

namespace Lab6.Models
{
    public class Game
    {
        private Board board;
        private char currentPlayer;
        private IPlayer playerX;
        private IPlayer playerO;

        public Game()
        {
            Console.WriteLine("Welcome to Tic Tac Toe!");
        }

        public void Play()
        {
            bool continuePlaying = true;
            while (continuePlaying)
            {
                InitializeGame();
                bool gameOn = true;

                while (gameOn)
                {
                    board.DisplayBoard();
                    Console.WriteLine($"Player {currentPlayer}'s turn.");

                    IPlayer currentPlayerObj = currentPlayer == 'X' ? playerX : playerO;
                    var move = currentPlayerObj.GetMove(board, currentPlayer);
                    board.MakeMove(move.Item1, move.Item2, currentPlayer);

                    if (board.CheckWin(currentPlayer))
                    {
                        board.DisplayBoard();
                        Console.WriteLine($"Player {currentPlayer} wins!");
                        gameOn = false;
                    }
                    else if (board.IsFull())
                    {
                        board.DisplayBoard();
                        Console.WriteLine("The game is a draw!");
                        gameOn = false;
                    }
                    else
                    {
                        currentPlayer = currentPlayer == 'X' ? 'O' : 'X';
                    }
                }

                continuePlaying = ShowEndMenu();
            }
        }

        private void InitializeGame()
        {
            board = new Board();
            currentPlayer = 'X';
            playerX = new HumanPlayer('X');
            playerO = SetupPlayer('O');
        }

        private IPlayer SetupPlayer(char player)
        {
            Console.WriteLine($"1. Play as {player}");
            Console.WriteLine($"2. Computer as {player}");

            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice) || (choice != 1 && choice != 2))
            {
                Console.WriteLine("Invalid choice. Please enter 1 or 2.");
            }

            if (choice == 2)
            {
                Console.WriteLine("Select difficulty:");
                Console.WriteLine("1. Easy");
                Console.WriteLine("2. Medium");
                Console.WriteLine("3. Hard");

                while (!int.TryParse(Console.ReadLine(), out choice) || (choice < 1 || choice > 3))
                {
                    Console.WriteLine("Invalid choice. Please enter 1, 2, or 3.");
                }

                switch (choice)
                {
                    case 1:
                        return new ComputerPlayer('O', new EasyAI(board));
                    case 2:
                        return new ComputerPlayer('O', new MediumAI(board));
                    case 3:
                        return new ComputerPlayer('O', new HardAI(board));
                }
            }

            return new HumanPlayer(player);
        }

        private bool ShowEndMenu()
        {
            Console.WriteLine("Game over!");
            Console.WriteLine("1. Play again");
            Console.WriteLine("2. Exit");

            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice) || (choice != 1 && choice != 2))
            {
                Console.WriteLine("Invalid choice. Please enter 1 or 2.");
            }

            return choice == 1;
        }
    }
}
