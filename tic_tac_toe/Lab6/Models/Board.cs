﻿using System;

namespace Lab6.Models
{
    public class Board
    {
        private Cell[,] _board;
        public Cell[,] board { get { return _board; }}

        public Board()
        {
            _board = new Cell[3, 3];
            InitializeBoard();
        }

        public Board(Cell[,] board)
        {
            _board = new Cell[3, 3];
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    _board[i, j] = new Cell { Value = board[i, j].Value };
                }
            }
        }

        private void InitializeBoard()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    _board[i, j] = new Cell();
                }
            }
        }

        public void DisplayBoard()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(_board[i, j].Value);
                    if (j < 2) Console.Write("|");
                }
                Console.WriteLine();
                if (i < 2) Console.WriteLine("-----");
            }
        }

        public bool MakeMove(int row, int col, char player)
        {
            if (_board[row, col].IsEmpty())
            {
                _board[row, col].Value = player;
                return true;
            }
            return false;
        }

        public bool UndoMove(int row, int col)
        {
            if (!_board[row, col].IsEmpty())
            {
                _board[row, col].Value = ' ';
                return true;
            }
            return false;
        }

        public bool CheckWin(char player)
        {
            for (int i = 0; i < 3; i++)
            {
                if (_board[i, 0].Value == player && _board[i, 1].Value == player && _board[i, 2].Value == player) return true;
                if (_board[0, i].Value == player && _board[1, i].Value == player && _board[2, i].Value == player) return true;
            }
            if (_board[0, 0].Value == player && _board[1, 1].Value == player && _board[2, 2].Value == player) return true;
            if (_board[0, 2].Value == player && _board[1, 1].Value == player && _board[2, 0].Value == player) return true;

            return false;
        }

        public bool IsFull()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (_board[i, j].IsEmpty()) return false;
                }
            }
            return true;
        }

        public bool IsEmpty(int row, int col)
        {
            return _board[row, col].IsEmpty();
        }
    }
}
