# Tic Tac Toe

## Опис проекту

Це консольна гра "Хрестики-нулики", написана на C#. Гравці по черзі ставлять хрестики або нулики на ігрове поле розміром 3x3. Мета гри - скласти три своїх символи в ряд, колонку або діагональ.

## Функціонал

- Ігрове поле 3x3.
- Перемикання гравців між ходами (X і O).
- Перевірка на перемогу.
- Перевірка на нічию.
- Вивід ігрового поля після кожного ходу.

## Programming Principles

1. **Single Responsibility Principle**: Кожен клас має єдину відповідальність. Клас `Cell` відповідає за зберігання стану клітинки, `Board` - за стан ігрового поля, `Game` - за логіку гри.
2. **Open/Closed Principle**: Класи відкриті для розширення, але закриті для модифікації. Наприклад, ми можемо додати нові функції до класів, не змінюючи їхньої поточної реалізації.
3. **Liskov Substitution Principle**: У цьому проекті цей принцип не застосовується, оскільки немає ієрархії наслідування.
4. **Interface Segregation Principle**: У цьому проекті ми не використовуємо інтерфейси, тому цей принцип не застосовується.
5. **Dependency Inversion Principle**: У цьому проекті ми не використовуємо залежності між модулями, тому цей принцип не застосовується.

## Design Patterns

1. **Factory Method**: Можна застосувати для створення об'єктів `Cell` в класі `Board`.
   - Файл: `Models/Board.cs`
   - Використовується для інкапсуляції логіки створення клітинок.
2. **Singleton**: Можна застосувати для зберігання єдиного екземпляра гри.
   - Файл: `Models/Game.cs`
   - Використовується для того, щоб забезпечити існування тільки одного екземпляра гри.
3. **Observer**: Можна застосувати для сповіщення про стан гри (перемога, нічия).
   - Файл: `Models/Game.cs`
   - Використовується для сповіщення гравців про результати.

## Refactoring Techniques

1. **Extract Method**: Виділення методів для зменшення дублювання коду. Наприклад, метод `CheckWin` перевіряє всі можливі варіанти перемоги.
2. **Rename Variable**: Зміна імен змінних для підвищення читабельності. Наприклад, зміна змінних `row` та `col` на більш осмислені назви.
3. **Introduce Parameter**: Введення параметрів для узагальнення функцій. Наприклад, передача символу гравця до методу `MakeMove`.

## Структура проекту

Lab6/
├── Models/
│   ├── Board.cs
│   ├── Cell.cs
│   ├── Game.cs
├── Program.cs
├── Lab6.csproj

